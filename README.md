# EM Token: The Electronic Money Token standard

Contributors: xxx

## Objective

The EM Token standard aims to enable the issuance of regulated electronic money on blockchain networks, and its practical usage in real financial applications.

## Background

Financial institutions work today with electronic systems which hold account balances in databases on core banking systems. In order for an institution to be allowed to maintain records of client balances segregated and available for clients, such institution must be regulated under a known legal framework and must possess a license to do so. Maintaining a license under regulatory supervision entails ensuring compliance (i.e. performing KYC on all clients and ensuring good AML practices before allowing transactions) and demonstrating technical and operational solvency through periodic audits, so clients depositing funds with the institution can rest assured that their money is safe.

There are only a number of potential regulatory license frameworks that allow institutions to issue and hold money balances for customers (be it retail corporate or institutional types). The most important and practical ones are three:
* **Electronic money entities**: these are leanly regulated vehicles that are mostly used today for cash and payments services, instead of more complex financial services. For example prepaid cards or online payment systems such as PayPal run on such schemes. In most jurisdictions, electronic money balances are required to be 100% backed by assets, which often entails hold cash on an omnibus account at a bank with 100% of the funds issued to clients in the electronic money ledger   
* **Banking licenses**: these include commercial and investment banks, which segregate client funds using current and other type of accounts implemented on core banking systems. Banks can create money by lending to clients, so bank money can be backed by promises to pay and other illiquid assets 
* **Central banks**: central banks hold balances for banks in RTGS systems, similar to core banking systems but with much more restricted yet critical functionality. Central banks create money by lending it to banks, which pledge their assets to central banks as a lender of last resort for an official interest rate

Regulations for all these types of electronic money are local, i.e. only valid for each jurisdiction and not valid in others. And regulations can vary dramatically in different jurisdictions - for example there are places with no electronic money frameworks, on everything has to be done through banking licenses or directly with a central bank. But in all cases compliance with existing regulation needs to ensured, in particular:
* **Know Your Customer (KYC)**: the institution needs to identify the client before providing her with the possibility of depositing money or transact. In different jurisdictions and for different types of licenses there are different levels of balance and activity that can be allowed for different levels of KYC. For example, low KYC requirements with little checks or even no checks at all can usually be acceptable in many jurisdictions if cashin balances are kept low (i.e. hundreds of dollars)
* **Anti Money Laundering (AML)**: the institution needs to perform checks of parties transacting with its clients, typically checking against black lists and doing sanction screening, most notably in the context international transactions

Beyond cash, financial instruments such as equities or bonds are also registered in electronic systems in most cases, although all these systems and the bank accounting systems are only connected through rudimentary messaging means, which leads to the need for reconciliations and manual management in many cases. Cash systems to provide settlement of transactions in the capital markets are not well connected to the transactional systems, and often entail delays and settlement risk

## Overview

The EM Token builds on Ethereum standards currently in use such as ERC20, but it extends them to provide few key additional pieces of functionality, needed in the regulated financial world:
* **Permissioning**: EM Token builds on HarborHQ's R-token construct, which uses a separate, upgradeable contract referenced through a registry to implement clearing logic for transfers. In the EM Token construct different permissioning mechanisms controlled by different roles are used to perform different operations to the ledger:
  * *KYC*: compliance departments (delegating in operations personnel) need mark wallets as KYC'ed to enable wallet owners to do transfers, redeems and other things. Different levels of KYC are supported with different sets of checks (e.g. different maximum balances or different cumulative cash in amounts in a given period)
  * *Hold*: arbitrary addresses (e.g. payments operator smart contracts or token exchange marketplaces) can be whitelisted to perform holds on wallets
  * *Credit*: a CRO or risk department is granted permission to change credit limits and whitelist token contracts as acceptable collateral     
* **Funding request**: users can ask for a wallet funding request by calling the smart contract and attaching a direct debit instruction string. The tokenizer reads this request, interprets the debit instructions, and triggers a transfer in the bank ledger to initiate the tokenization process  
* **Redeem**: users can request redemptions by calling the smart contract and attaching a payment instruction string. The (de)tokenizer reads this request, interprets the payment instructions, and triggers the transfer of funds (typically from the omnibus account) into the destination account, if possible
* **Holds**: token balances can be put on hold, which will make the held amount unavailable for further use until the hold is resolved (i.e. either executed, released by the payee, or expired after a timeout period). Holds are similar to escrows in that are firm and lead to final settlement
* **Credit lines**: an EM Token wallet can have associated a credit line, which is automatically drawn when transfers or holds are performed and there is insufficient balance in the wallet - i.e. the `transfer` method will then not throw if there is enough available credit in the wallet. Credit lines generate interest that is automatically accrued in the relevant associated token wallets 
* **Collateralization**: the credit lines described above can have a secured and an unsecured limit, and the secured limit can be connected to a collateral construct that takes tokens on hold until repayment date. Collateral instruments are marked-to-market periodically and their valuation can trigger execution actions related to the credit conditions 

The EM Token is thus different from other tokens commonly referred to as "stable coins" in that it is designed to be issued, burnt and made available to users in a compliant manner (i.e. with full KYC and AML compliance) through a licensed vehicle (an electronic money entity, a bank, or a central bank), and in that it provides the additional functionality described above so it can be used by other smart contracts implementing more complex financial applications such as interbank payments, supply chain finance instruments, or the creation of EM-Token denominated bonds and equities with automatic delivery-vs-payment

## Data structure and methods

Blah
```
Blah
```

## Implementation

Blah

## Future work

Blah


THIS IS THE SEED TO BUILD TYHE WORKGROUPS

