pragma solidity ^0.4.18;

interface IHoldableToken {


    modifier onlyOfficer() {
        _;
    }
    
    modifier onlyNotary() {
        _;
    }

    modifier onlyOperator() {
        _;
    }

    modifier onlyTarget(bytes32 holdId) {
        _;
    }

    modifier onlyOfficerOrOwner() {
        _;
    }
    
    //enum Status {PENDING, ACCEPTED, REVOKED_BY_EXPIRATION, REVOKED_BY_NOTARY, REVOKED_BY_TARGET, REVOKED_BY_REGULATOR}
    
    event HoldOrdered(bytes32 indexed holdId, address indexed origin, address indexed target, uint256 amount, address operator, uint256 expiration);

    event HoldReleased(bytes32 indexed holdId, address notary, address indexed origin, address indexed target, uint256 amount);

    event HoldRevoked(bytes32 indexed holdId, address notary, address indexed origin, address indexed target, uint256 amount, uint status);

    event HoldIdExists(bytes32 indexed holdId);

    event HoldExceededOper(address indexed operator,uint256 indexed maxhold, uint256 amount);

    function allowance(address owner, address spender) public view returns (uint256);

    function getTimePeriod() public view returns (uint256);

    function changeTimePeriod(uint256 _timePeriod) public onlyOfficerOrOwner;

    function heldBalanceOf(address account) public view returns (uint256 balance);

    function balanceOf(address account) public view returns (uint256 balance);

    function virtualBalanceOf(address account) public view returns (uint256 balance);

    /// @notice Hold an amount of tokens waiting to be released or revoked
    /// The hold id isn't used before and emit HoldCollisioned event.
    /// we need to check if the balance of the sender minus heldBalance it's enough to make the new hold.
    /// heldBalance[origin] += amount
    /// expiration = block.timestamp() + timePeriod
    function holdFrom(bytes32 holdId, address origin, address target, uint256 amount) public onlyOperator returns (uint256 expiration);


    /// @notice release a previous hold of tokens.
    /// Call to accept
    /// Make the transfer
    function releaseHold(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary);

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the owner.
    /// check if the timestamp of the block is upper than expiration.
    /// set the status to REVOKED_BY_EXPIRATION with _update. 
    function revokeByExpiration(bytes32 holdId) public onlyOfficerOrOwner returns (address origin, address target, uint256 amount, address agent, string status);


    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is in the whitelist of notaries.
    /// check if the timestamp of the block is less than expiration.
    /// set the status tu REVOKED_BY_NOTARY with _update.
    function revokeByNotary(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary, string status);

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the target of the operation.
    /// check if the timestamp of the block is less than expiration.
    /// set the status to REVOKED_BY_TARGET with _update.
    function revokeByTarget(bytes32 holdId) public onlyTarget(holdId) returns (address origin, address target, uint256 amount, address agent, string status);

    function _hold(bytes32 holdId, address origin, address target, uint256 amount) public onlyOperator returns (uint256 expiration);

    /// @notice release a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// check if the notary of the hold is the msg.sender.
    /// check if the timestamp of the block is less than expiration.
    /// heldBalance[origin] -= amount
    /// status to ACCEPTED with _update
    function accept(bytes32 holdId) public returns (address origin, address target, uint256 amount, address operator, address notary);

    /// @notice update a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// heldBalance[origin] -= amount
    /// status to the attribute status.
    function _update (bytes32 holdId, string status) public returns (address origin, address target, uint256 amount, address operator, address agent);

    function _obtainHold(bytes32 holdId) public view returns (address origin, address target, uint256 amount, address operator, address notary);
}