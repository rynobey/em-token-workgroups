pragma solidity ^0.5.0;

import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./INotary.sol";
import "./IOfficer.sol";
import "./IOperator.sol";


contract HoldableToken is INotary, IOfficer, IOperator, ERC20, Ownable {

    using SafeMath for uint256;

    enum Status {PENDING, ACCEPTED, REVOKED_BY_EXPIRATION, REVOKED_BY_NOTARY, REVOKED_BY_TARGET, REVOKED_BY_REGULATOR}
    enum FailHoldIssue {EXCEEDED_HOLD, NOT_ENOUGH_BALANCE, INACTIVE_OPER}

    struct HoldSt {
        address origin;
        address target;
        uint256 expiration;
        uint256 amount;
        address operator;
        address notifier;  //??? antes notifier  (PENDING => operator // EXPIRED=> Notifier, Officer, owner // REVOKED_BY_TARGET => target //REVOKED_BY_REGULATOR => Notifier)
        Status status;
    }
   

    mapping(bytes32 => HoldSt) private holds;
    mapping(address => uint256) private heldBalance;
   
    mapping (address => mapping (address => uint256)) private _allowed;

    address public officer;

    uint256 private timePeriod;   

    modifier onlyTarget(bytes32 holdId) {
        require(msg.sender == holds[holdId].target, "only the target of the hold can access this function");
        _;
    }

    //3
    modifier onlyOfficerOrOwner() {
        require ( (msg.sender == officer) || (msg.sender == owner), "the provided address is not a manager");
        _;
    }

    //3
    modifier onlyOfficer() {
        require ( (msg.sender == officer) , "the provided address is not a officer");
        _;
    }


    //2
    constructor(uint256 _timePeriod, address _officer) public {
        timePeriod = _timePeriod;
        numActiveNotaries = 0;
        numActiveOperators = 0;
        officer = _officer;
        if (officer == address(0) ){
            officer = owner;
        }    
    }

   
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowed[owner][spender];
    }

    /// @notice returns the current time period
    function getTimePeriod() public view returns (uint256) {
        return timePeriod;
    }

    /// @notice it changes the current timePeriod
    function changeTimePeriod(uint256 _timePeriod) public onlyOwner {
        timePeriod = _timePeriod;
    }

    /// @notice Retrieve the balance who was held.
    /// It contains only the amount that is currently held for this address.
    function heldBalanceOf(address account) public view returns (uint256 balance) {
        balance = heldBalance[account];
    }

    /// @notice Retrive the erc20.balanceOf(msg.sender) - heldBalance(msg.sender).
    function balanceOf(address account) public view returns (uint256 balance) {
        balance = virtualBalanceOf(account).sub(heldBalanceOf(account));
    }

    /// @notice Retrive the erc20.balanceOf(msg.sender)  (** TALK THIS CONCEPT WITH JULIO **)
    function virtualBalanceOf(address account) public view returns (uint256 balance) {
        balance = super.balanceOf(account);
    }

    
    function hold(
        bytes32 holdId,
        address target,
        uint256 amount,
        address notary
    ) public onlyOperator returns (uint256 expiration)
    {
        return _hold(
            holdId,
            msg.sender,
            target,
            amount,
            notary);
    }

    
    /// @notice Hold an amount of tokens waiting to be released or revoked
    /// The hold id isn't used before and emit HoldCollisioned event.
    /// we need to check if the balance of the sender minus heldBalance it's enough to make the new hold.
    /// heldBalance[origin] += amount
    /// expiration = block.timestamp() + timePeriod
    function holdFrom(
        bytes32 holdId,
        address origin,
        address target,
        uint256 amount,
        address notary
    ) public onlyOperator returns (uint256 expiration)
    {
        return _hold(
            holdId,
            origin,
            target,
            amount
            notary
        );
    }

    /// @notice release a previous hold of tokens.
    /// Call to _accept
    /// Make the transfer
    function releaseHold(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary) {
        
        holds[holdId].notary = msg.sender;
        (origin, target, amount, notary) = _accept(holdId);  
        balances[origin] = balances[origin].sub(amount);
        balances[target] = balances[target].add(amount);
        emit Transfer(origin, target, amount);
        emit HoldReleased(
            holdId,
            notary,
            origin,
            target,
            amount,
            msg.sender
        );
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the owner.
    /// check if the timestamp of the block is upper than expiration.
    /// set the status to REVOKED_BY_EXPIRATION with _update.
    
    function revokeByExpiration(bytes32 holdId) public onlyOfficerOrOwner returns (
        address origin,
        address target,
        uint256 amount,
        address operator,
        address notary, 
        Status status
    )
    {
        /* solium-disable-next-line security/no-block-members */
        require(block.timestamp >= holds[holdId].expiration, "it can only be revoked by owner if the hold has expired");
        status = Status.REVOKED_BY_EXPIRATION;
        (origin, target, amount, operator, notifier) = _update(holdId, Status.REVOKED_BY_EXPIRATION);
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is in the whitelist of notaries.
    /// check if the timestamp of the block is less than expiration.
    /// set the status tu REVOKED_BY_NOTARY with _update.
    function revokeByNotary(bytes32 holdId) public onlyNotary returns (
        address origin, 
        address target, 
        uint256 amount, 
        address operator,
        address notary, 
        Status status
        ) {
        /* solium-disable-next-line security/no-block-members */
        require(holds[holdId].expiration > 0 && block.timestamp < holds[holdId].expiration, "hold must not be expired");
        require(holds[holdId].agent == msg.sender, "sender must be the notary of the hold");

        status = Status.REVOKED_BY_NOTARY;
        (origin, target, amount, operator, notary) = _update(holdId, status);
        
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the target of the operation.
    /// check if the timestamp of the block is less than expiration.
    /// set the status to REVOKED_BY_TARGET with _update.
    function revokeByTarget(bytes32 holdId) public onlyTarget(holdId) returns (
        address origin,
        address target,
        uint256 amount,
        address operator,
        address notary, 
        Status status
    )
    {
        /* solium-disable-next-line security/no-block-members */
        require(holds[holdId].expiration > 0 && block.timestamp < holds[holdId].expiration, "hold must not be expired");
        status = Status.REVOKED_BY_TARGET;
        (origin, target, amount, operator, notary) = _update(holdId, status);
    }

    function _hold(
        bytes32 holdId,
        address origin,
        address target,
        uint256 amount
    ) internal onlyOperator returns (uint256 expiration)
    {
        if (holds[holdId].expiration != 0) {
            emit HoldIdExists(holdId);
        } else {
            assert(amount <= balanceOf(origin));
            assert(checkHoldOperator(msg.sender, amount));
            holds[holdId].origin = origin;
            holds[holdId].target = target;
            holds[holdId].amount = amount;
            holds[holdId].operator = msg.sender;
            holds[holdId].status = Status.PENDING;
   
            /* solium-disable-next-line security/no-block-members */
            expiration = block.timestamp.add(timePeriod);
            holds[holdId].expiration = expiration;
            heldBalance[origin] = heldBalance[origin].add(amount);
            emit HoldOrdered(
                holdId,
                origin,
                target,
                amount,
                msg.sender, //operator
                expiration
            );
        }
    }

    /// @notice release a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// check if the notary of the hold is the msg.sender.
    /// check if the timestamp of the block is less than expiration.
    /// heldBalance[origin] -= amount
    /// status to ACCEPTED with _update
    function _accept(bytes32 holdId) internal returns (address origin, address target, uint256 amount, address operator, address notary) {
        /* solium-disable-next-line security/no-block-members */
        require(block.timestamp < holds[holdId].expiration, "hold must not be expired");
        require(holds[holdId].agent == msg.sender, "sender must be the notifier of the hold");

        (origin, target, amount, operator, notary) = _update(holdId, Status.ACCEPTED);
    }

    /// @notice update a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// heldBalance[origin] -= amount
    /// status to the attribute status.

    function _update (bytes32 holdId, Status status) internal returns (address origin, address target, uint256 amount, address operator, address notary) {
        require(holds[holdId].status == Status.PENDING, "a hold must be in status pending to be updated");
        origin = holds[holdId].origin;
        target = holds[holdId].target;
        amount = holds[holdId].amount;
        operator = holds[holdId].operator;
        notary = holds[holdId].notary;
        holds[holdId].status = status;
        heldBalance[origin] = heldBalance[origin].sub(amount);
    }
    //public or private??
    function obtainHold(bytes32 holdId) public view returns (address origin, address target, uint256 amount, address operator, address notary, Status status) {
        origin = holds[holdId].origin;
        target = holds[holdId].target;
        amount = holds[holdId].amount;
        operator = holds[holdId].operator;
        notary = holds[holdId].notary;
        status = holds[holdId].status;
    }
}
