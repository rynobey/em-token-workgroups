pragma solidity ^0.5.0;

import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";


contract HoldableToken is ERC20, Ownable {

    using SafeMath for uint256;

    enum Status {PENDING, ACCEPTED, REVOKED_BY_EXPIRATION, REVOKED_BY_NOTARY, REVOKED_BY_TARGET, REVOKED_BY_REGULATOR}
    enum FailHoldIssue {EXCEEDED_HOLD, NOT_ENOUGH_BALANCE, INACTIVE_OPER}

    struct HoldSt {
        address origin;
        address target;
        uint256 expiration;
        uint256 amount;
        address operator;
        address agent;  //??? antes notifier  (PENDING => operator // EXPIRED=> Notifier, Officer, owner // REVOKED_BY_TARGET => target //REVOKED_BY_REGULATOR => Notifier)
        Status status;
    }

    struct NotarySt {
        address notary;
        bool actived;        //1
        uint256 creationDate;
        uint256 updateDate;
        uint16 id;  //id =0 => No active notary
    }
    //For RBAC we can do this: 
    // mapping(address => bool) private notaries;
    // function addNotary(address _notary) public onlyOwner {
    //     notaries[_notary] = true;
    // }

    // function removeNotary(address _notary) public onlyOwner {
    //     notaries[_notary] = false;
    // }

    // function isNotary(address _notary) public view returns (bool) {
    //     return notaries[_notary];
    // }

    struct OperatorSt {
        address operator;
        bool actived;        //1
        uint256 maxHold;
        uint256 currentHold;
        uint256 creationDate;
        uint256 updateDate;
        uint16 id;  //id => No active  operator
    }
    //For RBAC we can do this: 
    //mapping(address => bool) private operators;
    //mapping(address => uint256) private maxHoldOperator
    // function addOperator(address _operator) public onlyOfficer {
    //     notaries[_operator] = true;
    // }

    // function removeOperator(address _operator) public onlyOfficer {
    //     notaries[_operator] = false;
    // }

    // function isOperator(address _operator) public view returns (bool) {
    //     return notaries[_operator];
    // }

    mapping(bytes32 => HoldSt) private holds;
    mapping(address => uint256) private heldBalance;
    mapping(address => NotarySt) private notaries;
    mapping(address => OperatorSt) private operators;
    mapping (address => mapping (address => uint256)) private _allowed;
    

    // id of 1 to max(unit16)
    mapping(uint16 => address) private activeNotaries;
    mapping(uint16 => address) private activeOperators;

    address public officer;

    uint16 private numActiveNotaries;
    uint16 private numActiveOperators;


    uint256 private timePeriod;

    event HoldOrdered(
        bytes32 indexed holdId,
        address indexed origin,
        address indexed target,
        uint256 amount,
        uint256 operator,
        uint256 expiration
    );

    event HoldReleased(
        bytes32 indexed holdId,
        address notary,
        address indexed origin,
        address indexed target,
        uint256 amount,
        uint256 notifier
    );

    event HoldRevoked(
        bytes32 indexed holdId,
        address notary,
        address indexed origin,
        address indexed target,
        uint256 amount,
        uint256 notifier,
        Status status
    );

    event HoldIdExists(
        bytes32 indexed holdId
    );

    // NEW
    event NotaryAdded(
        address indexed notary
    );

    // NEW
    event NotaryRemoved(
        address indexed notary
    );

    //NEW 
    event NotaryRemovedFail(
        address indexed notary
    );

    // NEW
    event OperatorAdded(
        address indexed notary,
        uint256 indexed maxHold
    );

     // NEW
    event OperatorHoldUpdated(
        address indexed notary,
        uint256 indexed maxHold
    );

    // NEW
    event OperatorRemoved(
        address indexed notary
    );

    //NEW 
    event OperatorRemovedFail(
        address indexed operator
    );


    //NEW
    event HoldExceededOper(
        address indexed operator,
        uint256 indexed maxhold,
        uint256 indexed currenthold,
        uint256 amount
    );
   
    event OfficerChanged(
        address indexed originator,
        address indexed newOfficer
    );

    //NEW
   

    modifier onlyNotary() {
        require(isNotary(msg.sender), "the provided address has to be a notary");
        _;
    }
    
    //NEW
    modifier onlyOperator() {
        require(isOperator(msg.sender), "the provided address has to be an operator");
        _;
    }


    modifier onlyTarget(bytes32 holdId) {
        require(msg.sender == holds[holdId].target, "only the target of the hold can access this function");
        _;
    }

    //3
    modifier onlyOfficerOrOwner() {
        require ( (msg.sender == officer) || (msg.sender == owner), "the provided address is not a manager");
        _;
    }

    //3
    modifier onlyOfficer() {
        require ( (msg.sender == officer) , "the provided address is not a officer");
        _;
    }


    //2
    constructor(uint256 _timePeriod, address _officer) public {
        //notaries[msg.sender] = true;
        //notaries[msg.sender].actived = true;
        timePeriod = _timePeriod;
        numActiveNotaries = 0;
        officer = _officer;
        if (officer == address(0) ){
            officer = owner;
        }    
    }

   // 3
   function changeOfficer(address _newOfficer) public onlyOfficerOrOwner{
       officer = _newOfficer;
       emit OfficerChanged(msg.sender, _newOfficer);
    }

    function addNotary( NotarySt memory _notaryst) public onlyOfficer {
        address notary = _notaryst.notary;
        numActiveNotaries++;
        notaries[notary] = NotarySt(notary, true, block.timestamp, block.timestamp, numActiveNotaries);
        activeNotaries[numActiveNotaries] = notary;
        emit NotaryAdded(notary);
    }

    //NEW 
    function addOperator(OperatorSt memory _operatorst) public onlyOfficer {
        address operator = _operatorst.operator;
        numActiveOperators++;
        operators[operator] = OperatorSt(operator, true, operator.maxHold, 0, block.timestamp, block.timestamp, numActiveOperators);
        activeOperators[numActiveOperators] = operator;
        emit OperatorAdded(operator, operator.maxHold);
    }


    //function removeNotary(address _notary) public onlyOwner {
    //    notaries[_notary] = false;
    //}
    function removeNotary(address _notary) public onlyOfficer {
        if (!isNotary(_notary) )
            return;
        
        if (activeNotaries == 1) {
            emit NotaryRemovedFail(_notary, activeNotaries);
            return;
        }
        
        uint16 prevId;
        address  lastActiveNotary;
        
        // We change the id from last notary to the recent removed id 
        prevId = notaries[_notary].id;
        lastActiveNotary = activeNotaries[numActiveNotaries];
        notaries[lastActiveNotary].id = prevId;
        activeNotaries[prevId] = lastActiveNotary;

        notaries[_notary].actived = false;
        notaries[_notary].updateDate = block.timestamp;
        notaries[_notary].id = 0;
        numActiveNotaries--;
        
        emit NotaryRemoved(_notary);
    }

    //NEW 
    function removeOperator(address _operator) public onlyOfficer {
        if (!isOperator(_operator) )
            return;

        if (numActiveOperators == 1) {
            emit OperatorRemovedFail(_operator);
            return;
        }
  
        uint16 prevId;
        address  lastActiveOperator;

        // We change the id from last notary to the recent removed id 
        prevId = operators[_operator].id;
        lastActiveOperator = activeNotaries[numActiveOperators];
        notaries[lastActiveOperator].id = prevId;
        activeNotaries[prevId] = lastActiveOperator;

        operators[_operator].actived = false;
        operators[_operator].updateDate = block.timestamp;
        operators[_operator].id = 0;
        numActiveOperators--;
        emit OperatorRemoved(_operator);
    }


    function isNotary(address _notary) internal view returns (bool) {
        return notaries[_notary].actived;
    }

    //NEW 
    function isOperator(address _operator) internal view returns (bool) {
        return operators[_operator].actived;
    }

    //NEW 
    function isOperatorHoldable(address _operator, uint256 amount) internal view returns (bool) {
        if (  isOperator(_operator) &&
             ((operators[_operator].currentHold + amount) <= operators[_operator].maxHold)) {
            return true;
        }
        emit HoldExceededOper(_operator, operators[_operator].maxhold, operators[_operator].currenthold, amount);
    }

    //NEW 
    function updateHoldOperator(address _operator,uint256 _maxHold) public onlyOfficer  {
        return operators[_operator].maxHold =_maxHold;
    } 


    //NEW 
    function obtainNotary (address _notary) public view returns (address notary, bool actived,  uint256 creationDate, uint256 updateDate) {
        notary = notaries[_notary].notary; 
        actived = notaries[_notary].actived; 
        creationDate = notaries[_notary].creationDate;
        updateDate = notaries[_notary].updateDate;
    }

    //NEW 
    function obtainOperator (address _operator) public view returns(address operator, bool actived, uint256 maxHold, uint256 currentHold, uint256 creationDate, uint256 updateDate) {
        operator = operators[_operator].operator;
        actived = operators[_operator].actived;
        maxHold = operators[_operator].maxHold;
        currentHold = operators[_operator].currentHold;
        creationDate = operators[_operator].creationDate;
        updateDate = operators[_operator].updateDate;
    }


    // ???  _allowed[officer][operator] == operators[operator].maxHold  ???
    //NEW to keep ERC"= funcionality
    // 
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowed[owner][spender];
    }


    /// @notice returns the current time period
    function getTimePeriod() public view returns (uint256) {
        return timePeriod;
    }

    /// @notice it changes the current timePeriod
    function changeTimePeriod(uint256 _timePeriod) public onlyOwner {
        timePeriod = _timePeriod;
    }

    /// @notice Retrieve the balance who was held.
    /// It contains only the amount that is currently held for this address.
    function heldBalanceOf(address account) public view returns (uint256 balance) {
        balance = heldBalance[account];
    }

    /// @notice Retrive the erc20.balanceOf(msg.sender) - heldBalance(msg.sender).
    function balanceOf(address account) public view returns (uint256 balance) {
        balance = virtualBalanceOf(account).sub(heldBalanceOf(account));
    }

    /// @notice Retrive the erc20.balanceOf(msg.sender)  (** TALK THIS CONCEPT WITH JULIO **)
    function virtualBalanceOf(address account) public view returns (uint256 balance) {
        balance = super.balanceOf(account);
    }

    
    // ????  En principio debemos considerar que el sender siempre va a ser un operator => el 'wallet' no puede directamente hacer un hold
    //
    //function hold(
    //    bytes32 holdId,
    //    address target,
    //    uint256 amount,
    //    address notary
    //) public returns (uint256 expiration)
    //{
    //    return _hold(
    //        holdId,
    //        msg.sender,
    //        target,
    //        amount,
    //        notary);
    //}

    
    /// @notice Hold an amount of tokens waiting to be released or revoked
    /// The hold id isn't used before and emit HoldCollisioned event.
    /// we need to check if the balance of the sender minus heldBalance it's enough to make the new hold.
    /// heldBalance[origin] += amount
    /// expiration = block.timestamp() + timePeriod
    function holdFrom(
        bytes32 holdId,
        address origin,
        address target,
        uint256 amount
        // address notary -> Ahora será el operator que será el msg.sender  ????
    ) public onlyOperator returns (uint256 expiration)
    {
        return _hold(
            holdId,
            origin,
            target,
            amount
            // notary -> Ahora será el operator que será el msg.sender  ????
        );
    }

    /// @notice release a previous hold of tokens.
    /// Call to accept
    /// Make the transfer
    function releaseHold(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary) {
        (origin, target, amount, notary) = accept(holdId);  
        balances[origin] = balances[origin].sub(amount);
        balances[target] = balances[target].add(amount);
        emit Transfer(origin, target, amount);
        emit HoldReleased(
            holdId,
            notary,
            origin,
            target,
            amount,
            msg.sender
        );
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the owner.
    /// check if the timestamp of the block is upper than expiration.
    /// set the status to REVOKED_BY_EXPIRATION with _update.
    
    function revokeByExpiration(bytes32 holdId) public onlyOfficerOrOwner returns (
        address origin,
        address target,
        uint256 amount,
        address agent, //???? antes : address notary, Ahora: Officer, Owner, Notifier
        Status status
    )
    {
        /* solium-disable-next-line security/no-block-members */
        require(block.timestamp >= holds[holdId].expiration, "it can only be revoked by owner if the hold has expired");
        status = Status.REVOKED_BY_EXPIRATION;
        (origin, target, amount, operator, agent) = _update(holdId, Status.REVOKED_BY_EXPIRATION);

        // ???  The operator is added since we need to discount the holded amount from his total
        operators[operator].currentHold -= amount;
        
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is in the whitelist of notaries.
    /// check if the timestamp of the block is less than expiration.
    /// set the status tu REVOKED_BY_NOTARY with _update.
    function revokeByNotary(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary, Status status) {
        /* solium-disable-next-line security/no-block-members */
        require(holds[holdId].expiration > 0 && block.timestamp < holds[holdId].expiration, "hold must not be expired");
        require(holds[holdId].agent == msg.sender, "sender must be the notary of the hold");

        status = Status.REVOKED_BY_NOTARY;
        (origin, target, amount, operator, notary) = _update(holdId, status);
        // ???  The operator is added since we need to discount the holded amount from his total
        operators[operator].currentHold -= amount;
    }

    /// @notice revoke a previous hold of tokens.
    /// check if msg.sender is the target of the operation.
    /// check if the timestamp of the block is less than expiration.
    /// set the status to REVOKED_BY_TARGET with _update.
    function revokeByTarget(bytes32 holdId) public onlyTarget(holdId) returns (
        address origin,
        address target,
        uint256 amount,
        address agent,
        Status status
    )
    {
        /* solium-disable-next-line security/no-block-members */
        require(holds[holdId].expiration > 0 && block.timestamp < holds[holdId].expiration, "hold must not be expired");
        status = Status.REVOKED_BY_TARGET;
        (origin, target, amount, operator, agent) = _update(holdId, status);
        // ???  The operator is added since we need to discount the holded amount from his total
        operators[operator].currentHold -= amount;
    }

    function _hold(
        bytes32 holdId,
        address origin,
        address target,
        uint256 amount
        //uint256 notifier
    ) internal onlyOperator returns (uint256 expiration)
    {
        if (holds[holdId].expiration != 0) {
            emit HoldIdExists(holdId);
        } else {
            assert(amount <= balanceOf(origin));
            assert(isOperatorHoldable(msg.sender, amount));
            holds[holdId].origin = origin;
            holds[holdId].target = target;
            holds[holdId].amount = amount;
            holds[holdId].operator = msg.sender;
            holds[holdId].agent = operator;

            // ??? Inicializamos el Hold.Status
            holds[holdId].status = Status.PENDING;

            // ??? Consideramos este hold aunque si se revoka se deshace la operacion
            // así, si hay varias operaciones de Hold no permitimos superar el máximo
            operators[_operator].currenthold += amount;

            /* solium-disable-next-line security/no-block-members */
            expiration = block.timestamp.add(timePeriod);
            holds[holdId].expiration = expiration;
            heldBalance[origin] = heldBalance[origin].add(amount);
            emit HoldOrdered(
                holdId,
                origin,
                target,
                amount,
                msg.sender, //operator
                expiration
            );
        }
    }

    /// @notice release a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// check if the notary of the hold is the msg.sender.
    /// check if the timestamp of the block is less than expiration.
    /// heldBalance[origin] -= amount
    /// status to ACCEPTED with _update
    function accept(bytes32 holdId) internal returns (address origin, address target, uint256 amount, address operator, address notary) {
        /* solium-disable-next-line security/no-block-members */
        require(block.timestamp < holds[holdId].expiration, "hold must not be expired");
        require(holds[holdId].agent == msg.sender, "sender must be the notifier of the hold");

        (origin, target, amount, operator, notary) = _update(holdId, Status.ACCEPTED);
    }

    /// @notice update a previous hold of tokens.
    /// check if the holdId is PENDING.
    /// heldBalance[origin] -= amount
    /// status to the attribute status.

    function _update (bytes32 holdId, Status status) internal returns (address origin, address target, uint256 amount, address operator, address agent) {
        require(holds[holdId].status == Status.PENDING, "a hold must be in status pending to be updated");
        origin = holds[holdId].origin;
        target = holds[holdId].target;
        amount = holds[holdId].amount;
        operator = holds[holdId].operator;
        agent = holds[holdId].agent;
        holds[holdId].status = status;
        heldBalance[origin] = heldBalance[origin].sub(amount);
    }

    function _obtainHold(bytes32 holdId) internal view returns (address origin, address target, uint256 amount, address operator, address notary) {
        origin = holds[holdId].origin;
        target = holds[holdId].target;
        amount = holds[holdId].amount;
        operator = holds[holdId].operator;
        notary = holds[holdId].notary;
    }
}
