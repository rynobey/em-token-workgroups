pragma solidity ^0.4.18;

import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./IOfficer.sol";
import "./IOperator.sol";
import "./IHold.sol";
contract HoldableToken is IOfficer, IOperator, IHold, ERC20, Ownable {
}