pragma solidity ^0.4.20;


 /// @title Interface Holdable token
 ///
 /// @dev Note: Important!! all modifiers defined in this interface must be implemented in the contract 
 /// The sequence of hold contract
 /// https://gitlab.com/iobuilders/em-token-workgroups/blob/master/Hold%20group/hold_sequence.puml
 ///
 /// This implementation emits additional Approval events, allowing applications to reconstruct the allowance status for
 /// all accounts just by listening to said events. Note that this isn't required by the specification, and other
 /// compliant implementations may not do it.
 

interface IHold {


    modifier onlyOfficer() {
        _;
    }
    
    modifier onlyNotary() {
        _;
    }

    modifier onlyOperator() {
        _;
    }

    modifier onlyTarget(bytes32 holdId) {
        _;
    }

    modifier onlyOfficerOrOwner() {
        _;
    }
    
    //enum Status {PENDING, ACCEPTED, REVOKED_BY_EXPIRATION, REVOKED_BY_NOTARY, REVOKED_BY_TARGET, REVOKED_BY_REGULATOR}
    
    ///@notice This event is emited in the hold and holdFrom function.
    event HoldOrdered(bytes32 indexed holdId, address indexed origin, address indexed target, uint256 amount, uint256 operator, uint256 expiration);

    ///@notice This event is emited in the releaseHold function.
    event HoldReleased(bytes32 indexed holdId, address notary, address indexed origin, address indexed target, uint256 amount, uint256 notifier);

    ///@notice This event is emited in the revokeByExpiration, revokeByNotary and revokeByTarget function.
    event HoldRevoked(bytes32 indexed holdId, address notary, address indexed origin, address indexed target, uint256 amount, uint256 notifier, unit status);

    ///@notice This event is emited in the hold and holdFrom function when the holdId already exists.
    event HoldIdExists(bytes32 indexed holdId);

    
    /// @dev Function to check the amount of tokens that an owner allowed to a spender.
    /// @param owner address The address which owns the funds.
    /// @param spender address The address which will spend the funds.
    /// @return A uint256 specifying the amount of tokens still available for the spender.
    function allowance(address owner, address spender) public view returns (uint256);

    
    /// @dev Get Hold time period
    /// @return A uint256 specifying the Hold time period. 
    function getTimePeriod() public view returns (uint256);

    
    /// @dev Change the time period
    /// @param _timePeriod An uint256 specifying the Hold time period.
    function changeTimePeriod(uint256 _timePeriod) public onlyOfficerOrOwner;

    
    /// @dev Gets the currently held balance of the specified address.
    /// @param account The address to query the held balance of.
    /// @return An uint256 representing the held amount owned by the passed address.
    function heldBalanceOf(address account) public view returns (uint256 balance);

    
    /// @dev Gets the erc20.balanceOf(address account) - heldBalance(address account) of the specified address.
    /// @param account The address to query the balance of.
    /// @return An uint256 representing the amount owned by the passed address.
    function balanceOf(address account) public view returns (uint256 balance);

    /// @dev Gets the total balance (erc20.balanceOf(address account) of the specified address.
    /// @param account The address to query the balance of.
    /// @return An uint256 representing the amount owned by the passed address.
    function virtualBalanceOf(address account) public view returns (uint256 balance);

    /// @notice This Hold is made by the user
    /// @dev Hold an amount of tokens waiting to be released or revoked
    /// If the hold id isn't used before and emit HoldCollisioned event.
    /// In this case the origin is the msg.sender
    /// we need to check if the balance of the sender minus heldBalance it's enough to make the new hold.
    /// heldBalance[msg.sender] += amount
    /// expiration = block.timestamp() + timePeriod
    /// @param holdId A bytes32 which is the hold operation UUID. 
    /// @param target Target hold address.
    /// @param amount An uint256 representing the amount to be hold.
    /// @param notary The hold notary address. This notary is the one who should do the holdRelease or holdRevoke
    /// @return An uint256 representing the hold expiration date.
    function hold(bytes32 holdId, address target, uint256 amount, address notary) public onlyOperator returns (uint256 expiration);
    
    /// @notice This Hold is made by the operator to a third party
    /// @dev Hold an amount of tokens waiting to be released or revoked
    /// If the hold id isn't used before and emit HoldCollisioned event.
    /// we need to check if the balance of the sender minus heldBalance it's enough to make the new hold.
    /// heldBalance[origin] += amount
    /// expiration = block.timestamp() + timePeriod
    /// @param holdId A bytes32 which is the hold operation UUID. 
    /// @param origin Origin hold address which will increase the heldBalance.
    /// @param target Target hold address.
    /// @param amount An uint256 representing the amount to be hold.
    /// @param notary The hold notary address. This notary is the one who should do the holdRelease or holdRevoke.
    /// @return An uint256 representing the hold expiration date.
    function holdFrom(bytes32 holdId, address origin, address target, uint256 amount, address notary) public onlyOperator returns (uint256 expiration);


    /// @dev release a previous hold of tokens.
    /// Call to accept
    /// Make the transfer
    /// @param holdId A bytes32 which is the hold operation UUID.
    /// @return (origin, target, amount, notary)
    /// @@origin Origin hold address
    /// @@target Target hold address.
    /// @@amount An uint256 representing the amount held.
    /// @@notary The hold notary address.
    function releaseHold(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address notary);

    /// @dev revoke a previous hold of tokens.
    /// check if msg.sender is the owner.
    /// check if the timestamp of the block is upper than expiration.
    /// set the status to REVOKED_BY_EXPIRATION with _update.
    /// @param holdId A bytes32 which is the hold operation UUID.
    /// @return (origin, target, amount, operator, notary, status)
    /// @@origin Origin hold address
    /// @@target Target hold address.
    /// @@amount An uint256 representing the amount held.
    /// @@operator The hold operator address. 
    /// @@notary The hold notary address.
    function revokeByExpiration(bytes32 holdId) public onlyOfficerOrOwner returns (address origin, address target, uint256 amount, address operator, address notary, uint status);


    /// @dev revoke a previous hold of tokens.
    /// check if msg.sender is in the whitelist of notaries.
    /// check if the timestamp of the block is less than expiration.
    /// set the status tu REVOKED_BY_NOTARY with _update.
    /// @param holdId A bytes32 which is the hold operation UUID.
    /// @return (origin, target, amount, operator, notary, status)
    /// @@origin Origin hold address
    /// @@target Target hold address.
    /// @@amount An uint256 representing the amount held.
    /// @@operator The hold operator address. 
    /// @@notary The hold notary address.
    function revokeByNotary(bytes32 holdId) public onlyNotary returns (address origin, address target, uint256 amount, address operator, address notary, uint status);

    /// @dev revoke a previous hold of tokens.
    /// check if msg.sender is the target of the operation.
    /// check if the timestamp of the block is less than expiration.
    /// set the status to REVOKED_BY_TARGET with _update.
    /// @param holdId A bytes32 which is the hold operation UUID. 
    /// @return (origin, target, amount, operator, notary, status)
    /// @@origin Origin hold address
    /// @@target Target hold address.
    /// @@amount An uint256 representing the amount held.
    /// @@operator The hold operator address. 
    /// @@notary The hold notary address.
    function revokeByTarget(bytes32 holdId) public onlyTarget(holdId) returns (address origin, address target, uint256 amount, address operator, address notary, uint status);

    /// @notice Get hold information by holdId
    /// @param holdId A bytes32 which is the hold operation UUID. 
    /// @return (origin, target, amount, operator, notary, status)
    /// @@origin Origin hold address
    /// @@target Target hold address.
    /// @@amount An uint256 representing the amount held.
    /// @@operator The hold operator address. 
    /// @@notary The hold notary address.
    function obtainHold(bytes32 holdId) public view returns (address origin, address target, uint256 amount, address operator, address notary, uint status);
}