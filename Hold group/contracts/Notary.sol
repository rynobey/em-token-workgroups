pragma solidity ^0.4.18;

contract Notary {
    
    modifier onlyNotary() {
      require(isNotary(msg.sender), "the provided address has to be a notary");
     _;
    }

    mapping(address => bool) private notaries;

    function addNotary(address _notary) public onlyOfficer {
        notaries[_notary] = true;
    }

    function removeNotary(address _notary) public onlyOfficer {
       if (!isNotary(_notary) )
            return;
        
        if (numActiveNotaries == 1) {
            emit NotaryRemovedFail(_notary);
            return;
        }
        
        notaries[_notary] = false;
        
        emit NotaryRemoved(_operator);
    }

    function isNotary(address _notary) public view returns (bool) {
        return notaries[_notary];
    }
       
    function _addNotary( address _notary) public onlyOfficer {
        numActiveNotaries++;
        emit NotaryAdded(_notary);
    }
}