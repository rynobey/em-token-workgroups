pragma solidity ^0.4.18;

/// @title Interface to interact with the Operator funcionality 
/// @author IOBuilder-Adhara Spain
/// @notice Operator corresponds to a role.  There will be various operators by each 'Holdable Token'
/// Operators are the only ones to order holds.
//  Operator will select the notary they consider who 'release or revoke' the hold ordered by the operator.
//  Operators will be registered, modified or removed by 'the officer' of the 'Holdable Token'
/// @dev Operators must be belong to a whitelist.
/// Notaries do not belong to whitelist.

interface IOperator {
    
    ///
    /// @notice Modifier with restriction: function called by only the officer.
    ///
    modifier onlyOfficer() {
        _;
    }
    
    ///
    /// @notice Event to indicate a new Operator has been added.
    /// @param officer The Officer address who has added this new Operator.
    /// @param operator The address of the recently added Operator.
    /// @param maxHold The maximun amount of hold that the Operator can order.
    ///
    event OperatorAdded(address indexed officer, address indexed operator, uint256 indexed maxHold);

    ///
    /// @notice Event to indicate which Operator has been removed.
    /// @param officer The Officer address who has removed the Operator.
    /// @param operator The address of the recently removed Operator.
    ///
    event OperatorRemoved(address indexed officer, address indexed operator);

    ///
    /// @notice Event to indicate which Operator could not be removed.
    /// @param officer The Officer address who has wanted to remove the Operator.
    /// @param operator The address of Operator to remove.
    ///
    event OperatorRemovedFail(address indexed officer, address indexed operator);

    event OperatorMaxHoldUpdated(address indexed officer, address indexed operator, uint256 prevMaxHold, uint256 indexed maxHold);

    event OperatorWasNotLocated(address indexed operator);
    
    event OperatorMaxHoldExcedeed(address indexed operator, uint256 indexed maxhold, uint256 indexed amount);

    ///
    /// @notice Function to add a new Operator.
    /// Restriction: The Officer is the only one abble to call it.
    /// Event emited: 'event OperatorAdded'
    /// @param operator The Operator address to add.
    /// @param maxHold The maximun amount of hold that the Operator can order.
    /// @dev previously it has been confirmed that the operator belongs to the whiltelist. 
    /// maxHold is defined being when operator is created.
    /// It is possible to modify it afterwards and affects to any hold this operator do.
    /// It can be different for each Operator.
    ///
    function addOperator(address _operator, uint256 maxHold) public onlyOfficer;

    ///
    /// @notice Function to remove a Operator.
    /// Restriction: The Officer is the only one abble to call it.
    /// Event emited: 'OperatorRemoved' in succescful case
    /// Event emited: 'OperatorRemovedFail' if it fails since this 'Operator' is the only existing one.
    /// @param operator The Operator address to remove.
    ///
    function removeOperator(address _operator) public onlyOfficer;

    function updateMaxHoldOperator(address _operator, uint256 _maxHold) public onlyOfficer;

    function isOperator(address _operator) public view returns (bool);

    //// Verify if the _operator isOperator and then amount < maxHold
    function checkHoldOperator(address _operator, uint256 amount) public view returns (bool);

    function operators() public view returns ([]address);
}