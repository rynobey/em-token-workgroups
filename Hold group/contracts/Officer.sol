pragma solidity ^0.4.18;

import "../../../../OpenZeppelin/openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract Officer is Ownable {

    address private officer;

    constructor(){
        officer = owner;
    }

    modifier onlyOfficerOrOwner() {
    require ( (msg.sender == officer) || (msg.sender == owner), "the provided address is not a manager");
        _;
    } 
   function changeOfficer(address _newOfficer) public onlyOfficerOrOwner{
       officer = _newOfficer;       
       emit OfficerChanged(msg.sender, _newOfficer);
    }
    function obtainOfficer() internal onlyOfficerOrOwner view returns (address){
        return officer;
    }

}