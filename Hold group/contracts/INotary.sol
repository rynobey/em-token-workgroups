pragma solidity ^0.4.18;

interface INotary {
    
    modifier onlyOfficer() {
        _;
    }

    event NotaryAdded(address indexed officer, address indexed notary);

    event NotaryRemoved(address indexed officer, address indexed notary);

    event NotaryRemovedFail(address indexed officer, address indexed notary);

    function addNotary(address _notary) public onlyOfficer;
    
    function removeNotary(address _notary) public onlyOfficer;
    
    function isNotary(address _notary) public view returns (bool);

    function notaries() public view returns ([]address);

}