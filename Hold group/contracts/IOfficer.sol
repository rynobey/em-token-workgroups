pragma solidity ^0.4.18;

/// @title Interface to interact with the Officer funcionality 
/// @author IOBuilder-Adhara Spain
/// @notice Officer corresponds to a role. Initally, officer will be the owner. Officer can manage, register and remove operators.
/// @dev Officer will unique for each holdable token.

interface IOfficer {
    
    ///
    /// @notice Modifier with restriction: function called by the officer or the owner who deployed the contract.
    ///
    modifier onlyOfficerOrOwner() {
        _;
    }
    
    ///
    /// @notice Event to indicate the Officer has been changed.
    /// @param sender The address who orders the change of the officer.
    /// @param prevOfficer The address corresponding to the previous officer.
    /// @param newOfficer The address corresponding to the new officer.
    ///
    event OfficerChanged(address indexed sender, address indexed prevOfficer, address indexed newOfficer);

    ///
    /// @notice Function to allow to change the officer. 
    /// Restrictived by the modifier : onlyOfficerOrOwner.
    /// Event emited: 'event OfficerChanged'
    /// @dev It is public for this version of compiler (0.4.18)
    /// @param New officer address
    /// 
    function changeOfficer (address _newOfficer) public onlyOfficerOrOwner;
    
    ///
    /// @notice Function to get the current Officer addrees 
    /// @dev the variable officer is private.
    /// @return officer address
    /// 
    function obtainOfficer() internal onlyOfficerOrOwner view returns (address);

}