pragma solidity ^0.4.18;

contract Notary {

    mapping(address => uint256) internal operators;

    function isOperator(address _operator) internal view returns (bool) {
       return ( operators[_operator] > 0 );
    }
    modifier onlyOperator() {
        require(isOperator(msg.sender), "the provided address has to be an operator");
        _;
    }

    function addOperator(address _operator, uint256 _maxHold) public onlyOfficer {
      numActiveOperators++;
      operators[_operator] = _maxHold;
      emit OperatorAdded(msg.sender, operator, _maxHold);
    }

    function removeOperator(address _operator) public onlyOfficer {
      if (!isOperator(_operator) )
          return;

      if (numActiveOperators == 1) {
          emit OperatorRemovedFail(msg.sender, _operator);
          return;
      }
  
      operators[_operator] = 0;
      numActiveOperators--;
      emit OperatorRemoved(msg.sender, _operator);
    }
    function checkHoldOperator(address _operator, uint256 amount) internal view returns (bool) {
      if (isOperator(_operator) && (operators[_operator] >= amount)) {
          return true;
      }
      emit OperatorMaxHoldExcedeed(_operator, operators[_operator], amount);
      return false;
    }

    function updateHoldOperator(address _operator,uint256 _maxHold) public onlyOfficer  {
      return operators[_operator] = _maxHold;
    } 
}