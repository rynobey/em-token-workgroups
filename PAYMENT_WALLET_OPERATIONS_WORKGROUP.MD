PAYMENT/ WALLET OPERATIONS WORKGROUP
================


As a workgroup we want to define and build an emoney token standard, to allow the
 interoperability between different financial Implementations.

https://github.com/AdharaProjects/em-token

In this workgroup, the focus is to define all the interfaces needed to allow
usual operations at em-token level, always maintaining ERC20 compatibility. Once
interfaces are approoved by the overall workgroups, a reference Implementation
must be developed.

Hierarchy, sequenenc diagrams, solidity doc, interfaces and detailed doc are the expected output.

Its key that all parameters of the inferfaces are defined, including the json
formats.

Please define a standard payout, redempton, 2steptranfer, debit and fund statuses, and the best way to have
verbose error codes.

Please define events for all given scenarios to be as much verbose as possible on tx success.

All operations need to have an externalId as paremeter to be able to concilliate with originating systems, like CBS, payment platforms etc

Methods to be implemented:

- Fund
- Redeem
- Payout
- Debit
- 2StepTransfer
---

**US1 - FUND**

As token Issuer/Bank I want to offer an orderFund method to a token holder, that allows a given token
holder, to fund its token wallet. It is called orderFund, because it always will
be done in 2 steps, being the token issuer the one that updates the final status
of this fund request.

Specs:
The banks/token issuer reads this request, interprets the payment instructions, and triggers the transfer of funds into the destination account, if possible.
Please define the parameters: externalFundId + amount + fundingMethod json, knowing that money origin it can be a given omnibus account, normal bank account or credit card.
Its key to investigate payment standards, W3C, ISO, etc to check if we can use it or adapt it

Please define:
- parameters: externalFundId + amount + fundingMethod json
- How Bank and tokenholder will interact to write and read payment instructions, being compliant with GDPR, and not sharing sensible information.
- Events
- Order fund statuses and error codes


Tech Specs:

---
**US2 - REDEEM**

As token Issuer/Bank I want to offer an orderRedeem method to a token holder, that allows him, to redeem money from its token wallet, to the default bank account defined on the bank, token issuer platform. It is called orderRedeem, because it always will be done in 2 steps, being the token issuer the one that updates the final status, of this redeem request. This request, has only the externalRedeemId and the amount as parameters.

Specs:
The banks/token issuer reads this request, interprets the redeem instructions, and triggers the transfer of funds into the destination account based on its stored info ( possibly based on  the account connector)if possible
Please define:
- parameters: externalRedeemId,amount
- Events
- Redepmtion statuses and error codes
- Hold interaction

Tech Specs:


---
**US3 - PAYOUT**

As token Issuer/Bank I want to offer an orderPayout method to a token holder, that allows a given token
holder, to payout money from its token wallet, to the specified bank account. It is called orderPayout, because it always willbe done in 2 steps, being the token issuer the one that updates the final status
of this payout request.

Specs:
The banks/token issuer reads this request, interprets the payout instructions, and triggers the transfer of funds into the destination , if possible.
Please define the parameters: externalPayoutId + amount + payOutDefiniton json, knowing that money destination it can be a given omnibus account, normal bank account or credit card.
Please define:
- parameters: externalPayoutId + amount + payOutDefiniton json
- Events
- How Bank and tokenholder will interact to write and read payment instructions, being compliant with GDPR, and not sharing sensible information.
- Payout statuses and error codes
- Hold interaction

Tech Specs:

---
**US4 - DEBIT**

As token Issuer/Bank I want to offer an orderDebit method to a token holder, that allows a given token
holder, to debit money from its token wallet, from a specified bank account, payment provider. It is called orderDebit, because it always willbe done in 2 steps, being the token issuer the one that updates the final status
of this redeem request.

Specs:
The banks/token issuer reads this request, interprets the debit instructions, and triggers the transfer of funds into the destination , if possible.
Please define the parameters: externalDebitId + amount + debitDefiniton json, knowing that money destination it can be a given omnibus account, normal bank account or credit card.
Please define:
- parameters: externalDebitId + amount + debitDefiniton json
- Events
- How Bank and tokenholder will interact to write and read payment instructions, being compliant with GDPR, and not sharing sensible information.
- Debit statuses and error codes
- Hold interaction

Tech Specs:

---

**US5 - 2STEPTRANSFER**


As token Issuer/Bank I want to offer an orderTransfer method to a token holder, that allows a given token
holder, to transfer money in two steps if need, with the allowance of a thrid party,  from its token wallet, to the specified wallet. It is called orderTransfer, because it always willbe done in 2 steps, being the token issuer the one that updates the final status
of this redeem request.

Specs:
The banks/token issuer/validator reads this request, interprets the transfer instructions, and triggers the transfer of funds into the destination , if possible.
Please define the parameters: externalTransferId + amount + notary.
Please define:
- parameters: externalDebitId + amount + debitDefiniton json
- Events
- Debit statuses and error codes
- Hold interaction

Tech Specs:

---
**US6 - STATUS CHECK**

As a token holder I want to be able to check which is the status of my redemption, payout or funding request. using the id I added on the tx.

___
**US7 - On BEHALF**

As a bank I want to be able to order the same methods as user, in behalfOf the user, if the user optedIn on letting me as a platform do fund, debit, payout,redeem or 2tsteptranfer.

___
